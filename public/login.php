﻿<?php

header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  // session_destroy();
  // Делаем перенаправление на форму.
  header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>

<p class="login">Введите свой логин и пароль: <br> </p>

<form action="" method="post">
  <input name="login" />
  <input type="password" name="pass" />
  <input type="submit" value="Войти" />
</form>

<?php
} // Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

  // TODO: Проверть есть ли такой логин и пароль в базе данных.
  // Выдать сообщение об ошибках.

  // Подключение к моей БД
  $user = 'u20346';
  $pass = '2134325';
  $db = new PDO('mysql:host=localhost;dbname=u20346', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

  try {

    $stmt = $db->prepare("SELECT * from app5 WHERE login=?");
    $stmt -> execute([ $_POST['login'] ]);

    $user_id = $stmt->fetch(PDO::FETCH_ASSOC);
   
    if ( empty(htmlspecialchars(trim($user_id['login']) )) ) {
      exit( "Кажется, Вы ввели неправильный логин и пароль.") ;
    } else {
      
      if ($user_id['password'] == $_COOKIE['password'] )
      {
        $_SESSION['login'] = $user_id['login'];
        $_SESSION['uid'] = $user_id['id'];
      }
      
    }

  } catch(PDOException $e){
    print('Error : ' . $e->getMessage()); 
    exit();
  }






  // Если все ок, то авторизуем пользователя.
  $_SESSION['login'] = htmlspecialchars(trim( $_POST['login']));
  // $_SESSION['password'] = $_POST['password'];
  // Записываем ID пользователя.
  $_SESSION['uid'] = 123;

  // Делаем перенаправление.
  header('Location: ./');
}
